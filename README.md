# Bitcoin Token Core

Bitcoin Token (BTCT) is a digital currency. It uses peer-to-peer technology to operate with no central authority or banks; managing transactions are carried out collectively by the network.